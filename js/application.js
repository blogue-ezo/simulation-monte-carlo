
class Application {
  target = null;
  engine = null;
  viewModel = null;
  piComputer = null;
  piEstimationRepository = null;
  closestEstimation = null;

  constructor() {
    this.engine = new Engine(CANVAS_ELEMENT_ID);
    this.piEstimationRepository = new PiEstimationRepository();
  }

  bootstrap() {
    this._initializeViewModel();
    this._initializePiComputer();
    this._initializeEngine();
  }
  
  onloop() {
    const dot = this.piComputer.nextDot();
    const color = dot.inCircle ? DOT_INSIDE_CIRCLE_COLOR : DOT_OUTSIDE_CIRCLE_COLOR;
    
    this.engine.drawDot(dot.x, dot.y, color);
    this._persistClosestEstimationIfNecessary();
    this._displayPiComputerValues();
  }
  
  _computeTargetCenterAndRadius() {
    const x = this.engine.canvas.width / 2;
    const y = this.engine.canvas.height / 2;
    const radius = this._min(this.engine.canvas.width, this.engine.canvas.height) / 2 - margin * 2;
  
    return {
      pos: { x, y },
      radius,
    };
  }
  
  
  _min(a, b) {
    return a < b ? a : b;
  }

  _displayPiComputerValues() {
    this.viewModel.setPi(this.piComputer.approximatePI());
    this.viewModel.setDotsInCircle(this.piComputer.dotsInCircle);
    this.viewModel.setTotalDots(this.piComputer.totalNumDot);
    this.viewModel.setClosestEstimation(this.piComputer.closestEstimation.value);
    this.viewModel.setDotsInCircleClosestEstimation(this.piComputer.closestEstimation.dotsInCircle);
    this.viewModel.setTotalDotsClosestEstimation(this.piComputer.closestEstimation.totalNumDot);
  }

  _persistClosestEstimationIfNecessary() {
    if (this.piComputer.closestEstimation.distance < this.closestEstimation.distance) {
      this.closestEstimation = Object.assign({}, this.piComputer.closestEstimation);
      this.piEstimationRepository.saveClosestEstimation(this.closestEstimation);
    }
  }

  _initializeViewModel() {
    this.viewModel = new ViewModel();
    this.viewModel.onclickStartButton = () => this.engine.start();
    this.viewModel.setPiRealValue(Math.PI);

    this.closestEstimation = this.piEstimationRepository.findClosestEstimation();
    this.viewModel.setClosestEstimation(this.closestEstimation.value);
    this.viewModel.setDotsInCircleClosestEstimation(this.closestEstimation.dotsInCircle);
    this.viewModel.setTotalDotsClosestEstimation(this.closestEstimation.totalNumDot);
  }

  _initializePiComputer() {
    this.target = this._computeTargetCenterAndRadius();
    this.piComputer = new MonteCarloPiComputer(this.target.pos.x, this.target.pos.y, this.target.radius);
    this.piComputer.reset(this.closestEstimation);
  }

  _initializeEngine() {
    this.engine.drawSquare(this.target.pos.x, this.target.pos.y, this.target.radius, SQUARE_COLOR);
    this.engine.drawCircle(this.target.pos.x, this.target.pos.y, this.target.radius, TARGET_COLOR);
  
    this.engine.onloop = () => this.onloop();
  }
}

