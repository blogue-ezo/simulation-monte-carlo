const PI_ELEMENT_ID = 'pi';
const DOTS_IN_CIRCLE_ELEMENT_ID = 'dotsInCircle';
const TOTAL_DOTS_ELEMENT_ID = 'totalDots';
const CANVAS_ELEMENT_ID = 'canvas';
const START_BUTTON_ELEMENT_ID = 'startButton';
const CLOSEST_ESTIMATION_ELEMENT_ID = 'closestEstimation';
const DOTS_IN_CIRCLE_CLOSEST_ESTIMATION_ELEMENT_ID = 'dotsInCircleClosestEstimation';
const TOTAL_DOTS_CLOSEST_ESTIMATION_ELEMENT_ID = 'totalDotsClosestEstimation';
const PI_REAL_VALUE_ELEMENT_ID = 'piRealValue';

const SQUARE_COLOR = "#1f1f1f";
const TARGET_COLOR = "#2f2f2f";
const DOT_INSIDE_CIRCLE_COLOR = "#00ff00";
const DOT_OUTSIDE_CIRCLE_COLOR = "#ff0000";