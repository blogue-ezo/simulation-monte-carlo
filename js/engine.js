const margin = 10;

class Engine {
  canvas = null;
  ctx = null;

  constructor(canvasElementId) {
this.canvas = document.getElementById(canvasElementId);
this.ctx = canvas.getContext("2d");
    this._fixDpi();
  }

  start() {
    this._loop();
  }

  drawSquare(x, y, radius, color) {
    this.ctx.beginPath();
    this.ctx.rect(x - radius, y - radius, radius * 2, radius * 2);
    this.ctx.fillStyle = color;
    this.ctx.fill();
  }

  drawCircle(x, y, radius, color) {
    this.ctx.beginPath();
    this.ctx.arc(x, y,radius, 0, 2 * Math.PI, false);
    this.ctx.fillStyle = color;
    this.ctx.fill();
  }

  drawDot(x, y, color) {
    this.ctx.beginPath();
    this.ctx.arc(x, y, 1, 0, 2 * Math.PI, false);
    this.ctx.fillStyle = color;
    this.ctx.fill();
  }

  _fixDpi() {
    const dpi = window.devicePixelRatio;
    const styleHeight = +getComputedStyle(canvas)
      .getPropertyValue("height")
      .slice(0, -2);
    const styleWidth = +getComputedStyle(canvas)
      .getPropertyValue("width")
      .slice(0, -2);

      this.canvas.setAttribute("height", styleHeight * dpi);
      this.canvas.setAttribute("width", styleWidth * dpi);
  }

  _loop() {
    this.onloop();
    requestAnimationFrame(this._loop.bind(this));  
  }
}
