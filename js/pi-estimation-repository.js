class PiEstimationRepository {
  findClosestEstimation() {
    const bestPiEstimationRaw = localStorage.getItem('closestPiEstimation');
    if (bestPiEstimationRaw) {
      return JSON.parse(bestPiEstimationRaw);
    } else {
      return {
        value: 0,
        dotsInCircle: 0,
        totalNumDot: 0,
        distance: Infinity,
      };
    } 
  }

  saveClosestEstimation(piEstimation) {
    localStorage.setItem('closestPiEstimation', JSON.stringify(piEstimation));
  }
}