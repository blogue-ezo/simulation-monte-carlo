class ViewModel {
  piElement = null;
  dotsInCircleElement = null;
  totalDotsElement = null;
  startButtonElement = null;
  closestEstimationElement = null;
  dotsInCircleClosestEstimationElement = null;
  totalDotsClosestEstimationElement = null;
  piRealValueElement = null;
  onclickStartButton = null;

  constructor() {
    this.piElement = document.getElementById(PI_ELEMENT_ID);
    this.dotsInCircleElement = document.getElementById(
      DOTS_IN_CIRCLE_ELEMENT_ID
    );
    this.totalDotsElement = document.getElementById(TOTAL_DOTS_ELEMENT_ID);
    this.startButtonElement = document.getElementById(START_BUTTON_ELEMENT_ID);
    this.closestEstimationElement = document.getElementById(
      CLOSEST_ESTIMATION_ELEMENT_ID
    );
    this.dotsInCircleClosestEstimationElement = document.getElementById(
      DOTS_IN_CIRCLE_CLOSEST_ESTIMATION_ELEMENT_ID
    );
    this.totalDotsClosestEstimationElement = document.getElementById(
      TOTAL_DOTS_CLOSEST_ESTIMATION_ELEMENT_ID
    );
    this.piRealValueElement = document.getElementById(PI_REAL_VALUE_ELEMENT_ID);

    this._bindEvents();
  }

  setPi(pi) {
    this.piElement.innerHTML = pi;
  }

  setDotsInCircle(dotsInCircle) {
    this.dotsInCircleElement.innerHTML = dotsInCircle;
  }

  setTotalDots(totalDots) {
    this.totalDotsElement.innerHTML = totalDots;
  }

  setClosestEstimation(closestEstimation) {
    this.closestEstimationElement.innerHTML = closestEstimation;
  }

  setDotsInCircleClosestEstimation(dotsInCircleClosestEstimation) {
    this.dotsInCircleClosestEstimationElement.innerHTML =
      dotsInCircleClosestEstimation;
  }

  setTotalDotsClosestEstimation(totalDotsClosestEstimation) {
    this.totalDotsClosestEstimationElement.innerHTML =
      totalDotsClosestEstimation;
  }

  setPiRealValue(piRealValue) {
    this.piRealValueElement.innerHTML = piRealValue;
  }

  _bindEvents() {
    this.startButtonElement.addEventListener("click", () =>
      this.onclickStartButton()
    );
  }
}
