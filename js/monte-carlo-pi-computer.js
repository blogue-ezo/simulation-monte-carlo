class MonteCarloPiComputer {
  dotsInCircle = 0;
  totalNumDot = 0;
  closestEstimation = {
    distance: Infinity,
    value: 0,
    dotsInCircle: 0,
    totalNumDot: 0,
  };

  constructor(centerX, centerY, radius) {
    this.centerX = centerX;
    this.centerY = centerY;
    this.radius = radius;
  }

  reset(closestEstimation) {
    this.dotsInCircle = 0;
    this.dotsInSquare = 0;
    this.closestEstimation = Object.assign({}, closestEstimation);
  }

  nextDot() {
    const x = Math.random() * this.radius * 2 + this.centerX - this.radius;
    const y = Math.random() * this.radius * 2 + this.centerY - this.radius;
    const inCircle = this._isDotInCircle(x, y);

    if (inCircle) {
      this.dotsInCircle++;
    }

    this.totalNumDot++;
    this._setRecordIfClosest();

    return { x, y, inCircle };
  }

  approximatePI() {
    return this.dotsInCircle / this.totalNumDot * 4.0;
  }

  _isDotInCircle(x, y) {
    const distance = Math.sqrt(
      Math.pow(x - this.centerX, 2) + Math.pow(y - this.centerY, 2)
    );
    return distance <= this.radius;
  }

  _setRecordIfClosest() {
    const piApproximation = this.approximatePI();
    const distanceWithPi = Math.abs(piApproximation - Math.PI);
    if (distanceWithPi < this.closestEstimation.distance) {
      this.closestEstimation.value = piApproximation;
      this.closestEstimation.distance = distanceWithPi;
      this.closestEstimation.dotsInCircle = this.dotsInCircle;
      this.closestEstimation.totalNumDot = this.totalNumDot;
    }
  }
}
